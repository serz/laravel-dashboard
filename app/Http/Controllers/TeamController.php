<?php

namespace App\Http\Controllers;
use App\Models\Team;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Gate;

class TeamController extends Controller
{
    public function __invoke(): View
    {
        Gate::authorize('viewAny', Team::class);

        return view('pages.teams');
    }
}

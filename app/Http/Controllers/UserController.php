<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    public function index()
    {
        Gate::authorize('viewAny', User::class);

        return view('pages.users');
    }

    public function store(StoreUserRequest $request)
    {
        Gate::authorize('create', User::class);
    }

    public function show(User $user)
    {
        Gate::authorize('view', $user);

        return view('pages.profile', [
            'user' => $user,
        ]);
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        Gate::authorize('update', $user);
    }

    public function destroy(User $user)
    {
        Gate::authorize('delete', $user);
    }
}

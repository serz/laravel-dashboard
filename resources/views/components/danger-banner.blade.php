<div {{ $attributes->merge(['class' => 'bg-red-500 text-white text-center py-2']) }}>
    {{ $slot }}
</div>

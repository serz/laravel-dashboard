<?php

namespace App\Http\Controllers;

use App\Enums\Team;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class MenuItemController extends Controller
{
    public function __invoke(Request $request): View
    {
        abort_if(!$request->user()->teamIs(Team::Admin), 403);

        return view('pages.menu-items');
    }
}

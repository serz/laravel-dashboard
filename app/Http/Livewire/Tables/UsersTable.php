<?php

namespace App\Http\Livewire\Tables;

use App\Models\User;
use Laravel\Jetstream\Jetstream;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class UsersTable extends DataTableComponent
{
    protected $model = User::class;
    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function columns(): array
    {
        $this->emit('reInit');

        return [
            Column::make('ID', 'id')
                ->sortable(),
            Jetstream::managesProfilePhotos()
                ? Column::make('Profile Photo', 'profile_photo_path')
                    ->view('table-column.avatar')
                : null,
            Column::make('Name')
                ->view('table-column.user-name')
                ->searchable()
                ->sortable(),
            Column::make('Email')
                ->searchable()
                ->sortable(),
            Column::make('Team', 'currentTeam.name')
                ->view('table-column.team'),
            Column::make('Created At')
                ->view('table-column.date-time')
                ->sortable(),
            Column::make('Actions', 'id')
                ->view('table-column.user-actions')
                ->sortable(),
        ];
    }

    public function customView(): string
    {
        return 'pages.add-user';
    }
}

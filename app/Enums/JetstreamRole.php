<?php

namespace App\Enums;

enum JetstreamRole: string
{
    case Admin = 'admin';
    case Editor = 'editor';
}

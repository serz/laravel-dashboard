<?php

namespace App\Helpers;

class HtmlHelper
{
    public static function link($routeName, $text, $attributes = []): string
    {
        return view('components.link', [
            'url' => route($routeName, $attributes),
            'text' => $text,
        ]);
    }
}

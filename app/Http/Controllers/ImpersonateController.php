<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\RedirectResponse;

class ImpersonateController extends Controller
{
    public function __invoke(User $user): RedirectResponse
    {
        session(['impersonate.originalId' => auth()->user()->getAuthIdentifier()]);
        auth('web')->login($user);
        auth('sanctum')->setUser($user);

        return redirect()->route('dashboard');
    }

    public function stop(UserRepository $userRepository): RedirectResponse
    {
        $originalId = session('impersonate.originalId');
        if (!$originalId) {
            return redirect()->route('dashboard');
        }

        auth('web')->logout();

        $user = $userRepository->getById($originalId);
        auth('web')->loginUsingId($originalId);
        auth('sanctum')->setUser($user);

        session()->forget('impersonate');

        return redirect()->route('dashboard');
    }
}

@props(['url', 'text'])

<a href="{{ $url }}" class="font-semibold text-indigo-700">
    {{ $text }}
</a>

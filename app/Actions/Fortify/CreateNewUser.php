<?php

namespace App\Actions\Fortify;

use App\Enums\JetstreamRole;
use App\Enums\Team as TeamEnum;
use App\Models\User;
use App\Repositories\TeamRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Laravel\Jetstream\Jetstream;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Create a newly registered user.
     *
     * @param array<string, string> $input
     * @throws ValidationException
     */
    public function create(array $input): User
    {
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),
            'terms' => Jetstream::hasTermsAndPrivacyPolicyFeature() ? ['accepted', 'required'] : '',
        ])->validate();

        return DB::transaction(function () use ($input) {
            return tap(User::create([
                'name' => $input['name'],
                'email' => $input['email'],
                'password' => Hash::make($input['password']),
                'timezone' => config('app.timezone'),
            ]), function (User $user) {
                $team = (new TeamRepository())->getById(TeamEnum::Client->value);
                $team->users()->save($user, [
                    'role' => JetstreamRole::Editor,
                ]);
                $user->switchTeam($team);
            });
        });
    }
}

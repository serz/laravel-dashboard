help:
	@echo "up       - Start the Sail environment"
	@echo "down     - Stop the Sail environment"
	@echo "bash     - Enter the Sail environment bash"
	@echo "cs       - Run code style checks and give PHP Insights"
	@echo "fresh    - Migrate fresh and seed the database"
	@echo "clear    - Clear and cache configurations and routes"

up:
	./vendor/bin/sail up -d
down:
	./vendor/bin/sail down

bash:
	./vendor/bin/sail bash

cs:
	./vendor/bin/pint --test
	./vendor/bin/phpinsights

fresh:
	./vendor/bin/sail php artisan migrate:fresh --seed

clear:
	./vendor/bin/sail artisan optimize
	./vendor/bin/sail artisan cache:clear
	./vendor/bin/sail artisan config:cache
	./vendor/bin/sail artisan route:cache

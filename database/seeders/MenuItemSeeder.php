<?php

namespace Database\Seeders;

use App\Enums\Team;
use App\Models\MenuItem;
use App\Repositories\TeamRepository;
use Illuminate\Database\Seeder;

class MenuItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(TeamRepository $teamRepository): void
    {
        $adminTeam = $teamRepository->getById(Team::Admin->value);
        $clientTeam = $teamRepository->getById(Team::Client->value);

        $dashboard = MenuItem::factory()->create([
            'name' => 'Dashboard',
            'route' => 'dashboard',
            'icon' => null,
        ]);
        $dashboard->teams()->save($adminTeam);
        $dashboard->teams()->save($clientTeam);

        $users = MenuItem::factory()->create([
            'name' => 'Users',
            'route' => 'users.index',
            'icon' => null,
        ]);
        $users->teams()->save($adminTeam);

        $teams = MenuItem::factory()->create([
            'name' => 'Teams',
            'route' => 'teams.index',
            'icon' => null,
        ]);
        $teams->teams()->save($adminTeam);

        $menuItems = MenuItem::factory()->create([
            'name' => 'Menu Items',
            'route' => 'menu-items.index',
            'icon' => null,
        ]);
        $menuItems->teams()->save($adminTeam);

        $tasks = MenuItem::factory()->create([
            'name' => 'Tasks',
            'route' => 'totem.tasks.all',
            'icon' => null,
        ]);
        $tasks->teams()->save($adminTeam);
    }
}

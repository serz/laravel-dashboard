<span data-tooltip-target="tooltip-impersonate-{{ $row->id }}" class="px-2">
    <a href="{{ route('impersonate', $value)}}">
        <i class="fa fa-sign-in" aria-hidden="true"></i>
    </a>
</span>
<x-tooltip id="tooltip-impersonate-{{ $row->id }}">
    Impersonate
</x-tooltip>

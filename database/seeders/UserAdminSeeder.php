<?php

namespace Database\Seeders;

use App\Enums\Team;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::factory()->create([
            'name' => 'Admin',
            'email' => 'admin@example.net',
            'password' => Hash::make('qwe123qwe'),
            'current_team_id' => Team::Admin,
        ]);
    }
}

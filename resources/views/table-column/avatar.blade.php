<a href="{{ route('users.show', $row->id) }}">
    <img src="{{ $row->profilePhotoUrl }}" alt="{{ $row->name }}" class="rounded-md h-10 w-10 object-cover" />
</a>

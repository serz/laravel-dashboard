<?php

namespace Database\Seeders;

use App\Enums\JetstreamRole;
use App\Enums\Team;
use App\Models\User;
use App\Repositories\TeamRepository;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(TeamRepository $teamRepository): void
    {
        $clientTeam = $teamRepository->getById(Team::Client->value);
        User::factory(50)->create()->each(function (User $user) use ($clientTeam) {
            $clientTeam->users()->save($user, [
                'role' => JetstreamRole::Editor,
            ]);
        });
    }
}

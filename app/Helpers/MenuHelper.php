<?php

namespace App\Helpers;

use App\Repositories\MenuItemRepository;
use Illuminate\Database\Eloquent\Collection;

class MenuHelper
{
    public static function items(): Collection
    {
        // todo Cache result
        return (new MenuItemRepository())->byTeamId(auth()->user()->currentTeam->id);
    }
}

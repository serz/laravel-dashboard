<?php

namespace Tests\Feature\Livewire;

use App\Http\Livewire\Tables\UsersTable;
use Livewire\Livewire;
use Tests\TestCase;

class UsersTableTest extends TestCase
{
    /** @test */
    public function the_component_can_render()
    {
        $component = Livewire::test(UsersTable::class);

        $component->assertStatus(200);
    }
}

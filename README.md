# Laravel Starter Kit

Laravel Starter Kit is a boilerplate project to kickstart your Laravel applications with pre-configured features such as authentication, user management, dynamic navigation based on user roles, and more.

## Features

- **Laravel Sail** for easy Docker containerization
- **Livewire** and **Tailwind CSS** for a modern and dynamic frontend
- **Dynamic Navigation** with role-based access control
- **User Timezone Support** for personalized user experiences
- **Pre-configured Code Quality Tools**

## Installation

1. Clone the repository:
   ```sh
   git clone git@gitlab.com:serz/laravel-dashboard.git
   ```
2. Change into the project directory:
   ```sh
   cd laravel-starter-kit
   ```
3. Install Composer dependencies:
   ```sh
   composer install
   ```
4. Install NPM dependencies:
   ```sh
   npm install && npm run build
   ```
5. Copy the `.env.example` file to create your own `.env` file:
   ```sh
   cp .env.example .env
   ```
6. Generate an app key:
   ```sh
   php artisan key:generate
   ```
7. Run the migrations and seed the database:
   ```sh
   php artisan migrate --seed
   ```
8. Start the Laravel Sail environment:
   ```sh
   ./vendor/bin/sail up
   ```
9. Storage folder link:
    ```sh
    php artisan storage:link
    ```

### Configure cronjob:
```sh
* * * * * php /path-to-your-project/artisan schedule:run >> /dev/null 2>&1
```

## Customizing colors
- **Color variations** - `https://tailwindcss.com/docs/customizing-colors`
- `resources/views/components/button.blade.php`
- `resources/views/components/nav-link.blade.php`
- `resources/views/components/responsive-nav-link.blade.php`

## Localization
- Set default timezone in `config/app.php`

## Usage

### Makefile Commands

- `make up`: Start the Sail environment
- `make down`: Stop the Sail environment
- `make bash`: Enter the Sail environment bash
- `make cs`: Run code quality checks
- `make fresh`: Migrate fresh and seed the database
- `make clear`: Clear and cache configurations and routes

### Configure teams
Add your teams (user groups) here: `app/Enums/Team.php`

### Tailwind CSS Tooltips

To use Tailwind CSS tooltips in your views, utilize the tooltip component like so:

```html
<i class="fa fa-user-group" data-tooltip-target="tooltip-id"></i>
<x-tooltip id="tooltip-id">
    Tooltip text
</x-tooltip>
```

### Code generation
Model:
```sh
php artisan code:models --table=[table_name]
```

## Contributing

Feel free to fork the project and submit your pull requests. We appreciate your contributions!

## License

This project is licensed under the MIT License.

## Contact

Your Name - sergei.kalinistov@gmail.com

Project Link: [https://gitlab.com/serz/laravel-dashboard](https://gitlab.com/serz/laravel-dashboard)
```

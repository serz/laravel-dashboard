<?php
/**
 * @var User $user
 */

use App\Models\User;
?>
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ $user->name }} Profile
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-lg mt-16">
                @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
                    <div class="w-full px-4 flex justify-center relative">
                        <img src="{{ $user->profile_photo_url }}"
                             alt="{{ $user->name }}"
                             class="shadow-xl rounded-full h-40 w-40 align-middle border-none absolute -mt-20">
                    </div>
                @endif

                <div class="text-center mt-20 py-8">
                    <h3 class="text-xl font-semibold leading-normal mb-2 text-blueGray-700 mb-2">
                        {{ $user->name }}
                    </h3>
                    <div class="text-sm leading-normal mt-0 mb-2 font-bold">
                        <i class="fa fa-clock mr-2 text-gray-700"></i>
                        {{ $user->timezone }}
                    </div>
                    <div class="text-sm leading-normal mt-0 mb-2 font-bold">
                        <i class="fa fa-envelope mr-2 gray-700"></i>
                        {{ $user->email }}
                    </div>
                    <div class="text-sm leading-normal mt-0 mb-2 font-bold">
                        <i class="fa fa-user mr-2 gray-700"></i>
                        {{ $user->currentTeam->name }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

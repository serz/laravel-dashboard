<?php
/**
 * @var Carbon $value
 */

use Carbon\Carbon;
?>

<span data-tooltip-target="tooltip-date-time-{{ $row->id }}">
    {{ $value->diffForHumans() }}
</span>
<x-tooltip id="tooltip-date-time-{{ $row->id }}">
    {{ $value->format('j M') }}
    {{ $value->isCurrentYear() ? '' :  ", {$value->format('Y')}" }}
    {{ $value->format('H:i') }}
</x-tooltip>

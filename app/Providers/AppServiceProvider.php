<?php

namespace App\Providers;

use App\Enums\Team;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Studio\Totem\Totem;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Totem::auth(fn () => Auth::check() && Auth::user()->teamIs(Team::Admin));
    }
}

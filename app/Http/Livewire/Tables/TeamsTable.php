<?php

namespace App\Http\Livewire\Tables;

use App\Enums\JetstreamRole;
use App\Models\Team;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class TeamsTable extends DataTableComponent
{
    public bool $searchStatus = false;
    public bool $paginationStatus = false;
    protected bool $columnSelectStatus = false;
    protected $model = Team::class;

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function columns(): array
    {
        return [
            Column::make('Id'),
            Column::make('Name')
                ->view('table-column.team-link'),
            Column::make('Admins', 'id')
                ->label(function (Team $team) {
                    return $team->users
                        ->where('membership.role', '=', JetstreamRole::Admin->value)
                        ->pluck('email')
                        ->implode(', ');
                }),
            Column::make('Total Users', 'id')
                ->label(fn (Team $team) => $team->users->count()),
            Column::make('Created At')
                ->view('table-column.date-time'),
        ];
    }
}

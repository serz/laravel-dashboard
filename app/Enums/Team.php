<?php

namespace App\Enums;

enum Team: int
{
    use HasArray;
    case Admin = 1;
    case Client = 2;
}

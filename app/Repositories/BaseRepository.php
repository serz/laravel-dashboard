<?php

declare(strict_types=1);

namespace App\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository
{
    protected Model $model;

    public function __construct()
    {
        $this->model = $this->getDataModelClass();
    }

    final public function getById(int $id): Model
    {
        return $this->getQuery()
            ->find($id);
    }

    final public function getQuery(): Builder
    {
        return $this->model::query();
    }

    final public function columnIndexedById(string $column): array
    {
        return $this->getQuery()
            ->orderBy('id')
            ->pluck($column, 'id')
            ->toArray();
    }

    /**
     * todo generic template https://phpstan.org/blog/generics-in-php-using-phpdocs
     * @return Model
     */
    private static function getDataModelClass(): Model
    {
        $namespace = str_replace(
            ['\\Repositories\\', 'Repository'],
            ['\\Models\\', ''],
            static::class
        );

        return new $namespace();
    }
}

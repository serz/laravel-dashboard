{{ $value }}

@if($row->teams->count() > 1)
    <i class="fa fa-user-group text-gray-500 text-xs" data-tooltip-target="tooltip-teams-{{ $row->id }}"></i>
    <x-tooltip id="tooltip-teams-{{ $row->id }}">
        {{ $row->teams->pluck('name')->implode(', ') }}
    </x-tooltip>
@endif

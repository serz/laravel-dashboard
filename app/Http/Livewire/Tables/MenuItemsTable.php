<?php

namespace App\Http\Livewire\Tables;

use App\Models\MenuItem;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class MenuItemsTable extends DataTableComponent
{
    public bool $searchStatus = false;
    public bool $paginationStatus = false;
    protected bool $columnSelectStatus = false;
    protected $model = MenuItem::class;

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function columns(): array
    {
        return [
            Column::make('Id')
                ->sortable(),
            Column::make('Order')
                ->sortable(),
            Column::make('Name')
                ->sortable(),
            Column::make('Parent', 'menu_item_id'),
            Column::make('Route')
                ->sortable(),
            Column::make('Teams')
                ->label(fn (MenuItem $item) => $item->teams->pluck('name')->implode(', ')),
            Column::make('Icon')->html(),
            Column::make('Created At')
                ->view('table-column.date-time'),
        ];
    }
}

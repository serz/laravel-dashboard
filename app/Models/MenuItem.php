<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class MenuItem
 *
 * @property int $id
 * @property int $menu_item_id
 * @property string $name
 * @property string $route
 * @property string|null $icon
 * @property int $order
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Team[] $teams
 * @property MenuItem $parent
 *
 * @package App\Models
 */
class MenuItem extends Model
{
    use HasFactory;

	protected $table = 'menu_items';

	protected $fillable = [
		'name',
		'route',
		'icon',
        'order',
	];

	public function teams(): BelongsToMany
    {
        return $this->belongsToMany(Team::class, 'menu_item_team');
	}

    public function parent(): BelongsTo
    {
        return $this->belongsTo(MenuItem::class, 'menu_item_id');
    }
}

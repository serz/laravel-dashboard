<?php

namespace App\Http\Middleware;

use App\Enums\Team;
use Closure;
use Illuminate\Http\Request;

class AdminMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if (auth()->check() && auth()->user()->teamIs(Team::Admin)) {
            return $next($request);
        }

        abort(403);
    }
}

<?php

namespace Database\Seeders;

use App\Enums\JetstreamRole;
use App\Enums\Team as TeamEnum;
use App\Models\Team;
use App\Repositories\UserRepository;
use Illuminate\Database\Seeder;

class TeamSeeder extends Seeder
{
    private const ADMIN_USER_ID = 1;
    private const TEAM_NAME_POSTFIX = "'s Team";

    /**
     * Run the database seeds.
     */
    public function run(UserRepository $userRepository): void
    {
        $admin = $userRepository->getById(self::ADMIN_USER_ID);

        foreach (TeamEnum::names() as $teamName) {
            $team = Team::factory()->create([
                'name' => $teamName . self::TEAM_NAME_POSTFIX,
                'user_id' => self::ADMIN_USER_ID,
            ]);
            $team->users()->save($admin, [
                'role' => JetstreamRole::Admin,
            ]);
        }
    }
}

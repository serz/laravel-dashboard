<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class MenuItemRepository extends BaseRepository
{
    public function byTeamId(int $teamId): Collection
    {
        return $this->getQuery()
            ->whereHas('teams', function (Builder $query) use ($teamId) {
                $query->where('team_id', $teamId);
            })->get();
    }
}

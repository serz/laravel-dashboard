<x-dialog-modal id="add-user-modal" wire:model="isAddUserDisplayed">
    <x-slot name="title">
        {{ __('Add User') }}
    </x-slot>

    <x-slot name="content">
        form here
    </x-slot>

    <x-slot name="footer">
        buttons here
    </x-slot>
</x-dialog-modal>
